package com.example.rizalsqlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    LayoutInflater inflater;
    Context _context;
    private AppDatabase appDb ;
    List<Item> item;


    public ItemAdapter(MainActivity context, List<Item> it) {
        this._context = context;
        this.item = it;
        this.inflater = LayoutInflater.from(this._context);
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Item i = item.get(position);
        String nama = i.judul;
        String nim = i.nim;

        holder.tvNama.setText(nama);
        holder.tvNim.setText(nim);
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tvNim;
        TextView tvNama;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNama = itemView.findViewById(R.id.tvNama);
        }
    }
    public void setUserList(List<Item> item) {
        this.item = item;
        notifyDataSetChanged();
    }

}
